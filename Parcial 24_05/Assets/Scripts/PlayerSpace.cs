using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerSpace : MonoBehaviour
{
    public Transform[] movementPoints;
    public float movementSpeed = 5f;
    public Button[] zoneButtons;

    private NavMeshAgent navMeshAgent;
    private Dictionary<Button, Transform> buttonToZoneMap = new Dictionary<Button, Transform>();

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = movementSpeed;

        // Asociar cada bot�n con su respectiva zona
        for (int i = 0; i < zoneButtons.Length; i++)
        {
            if (i < movementPoints.Length)
            {
                buttonToZoneMap.Add(zoneButtons[i], movementPoints[i]);
                int index = i; // Variable local para preservar el valor correcto de 'i'
                zoneButtons[i].onClick.AddListener(() => MoveToZone(movementPoints[index]));
            }
        }
    }

    private void MoveToZone(Transform destination)
    {
        navMeshAgent.SetDestination(destination.position);
    }
}
