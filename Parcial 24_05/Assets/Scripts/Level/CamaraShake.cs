using UnityEngine;
using UnityEngine.UI;

public class CamaraShake : MonoBehaviour
{
    public GameObject camara; // Referencia a la c�mara que deseas sacudir
    public AudioClip sonido; // Sonido a reproducir al activar el efecto de sacudida

    public float duracion = 0.1f; // Duraci�n total del efecto de sacudida
    public float intensidad = 0.5f; // Intensidad de la sacudida
    public float velocidad = 1.0f; // Velocidad a la que la c�mara se sacude

    private Vector3 posicionInicial; // Posici�n inicial de la c�mara
    private float tiempoInicio; // Tiempo de inicio del efecto de sacudida

    private AudioSource audioSource; // Referencia al componente AudioSource

    void Start()
    {
        // Obtener o agregar un componente AudioSource al objeto
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        // Asegurarse de que se haya asignado una c�mara
        if (camara == null)
        {
            Debug.LogError("El script BotonCamaraShake necesita una referencia a la c�mara que deseas sacudir.");
        }
        else
        {
            // Obtener la posici�n inicial de la c�mara
            posicionInicial = camara.transform.localPosition;
        }
    }

    void Update()
    {
        if (Time.time - tiempoInicio < duracion)
        {
            // Calcular la posici�n de sacudida en funci�n del tiempo
            float factorTiempo = (Time.time - tiempoInicio) * velocidad;
            Vector3 posicionSacudida = Random.insideUnitSphere * intensidad * factorTiempo;

            // Actualizar la posici�n de la c�mara
            camara.transform.localPosition = posicionInicial + posicionSacudida;
        }
        else
        {
            // Restaurar la posici�n inicial de la c�mara
            camara.transform.localPosition = posicionInicial;
        }
    }

    public void IniciarSacudida()
    {
        // Reproducir el sonido
        if (sonido != null)
        {
            audioSource.PlayOneShot(sonido);
        }


        // Establecer el tiempo de inicio del efecto de sacudida
        tiempoInicio = Time.time;
    }
    
}
