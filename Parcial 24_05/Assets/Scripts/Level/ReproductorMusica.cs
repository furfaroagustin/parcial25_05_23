using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ReproductorMusica : MonoBehaviour
{
    public AudioSource audioSource; // Componente AudioSource para reproducir m�sica
    public Dropdown musicDropdown; // Dropdown para seleccionar el tema musical
    public Slider volumeSlider; // Slider para controlar el volumen

    public List<AudioClip> musicClips; // Lista de clips de audio de los temas musicales

    private float maxVolume = 1f; // Volumen m�ximo del tema musical

    private void Start()
    {
        // Asignar los controladores de eventos a los componentes
        musicDropdown.onValueChanged.AddListener(ChangeMusic);
        volumeSlider.onValueChanged.AddListener(ChangeVolume);

        // Llenar el dropdown con las opciones de los temas musicales
        FillDropdown();
    }

    private void FillDropdown()
    {
        // Limpiar las opciones existentes del dropdown
        musicDropdown.ClearOptions();

        // Crear una lista de opciones para el dropdown
        List<string> musicOptions = new List<string>();

        // Agregar el nombre de cada tema musical a la lista de opciones
        foreach (AudioClip clip in musicClips)
        {
            musicOptions.Add(clip.name);
        }

        // Asignar las opciones al dropdown
        musicDropdown.AddOptions(musicOptions);
    }

    private void ChangeMusic(int index)
    {
        // Obtener el clip de audio seleccionado del dropdown
        AudioClip selectedClip = musicClips[index];

        // Asignar el clip de audio al AudioSource
        audioSource.clip = selectedClip;

        // Iniciar la reproducci�n del audio
        audioSource.Play();
    }

    private void ChangeVolume(float value)
    {
        // Calcular el volumen en funci�n del valor del slider y la regla de tres
        float volume = (value / volumeSlider.maxValue) * maxVolume;

        // Establecer el volumen del AudioSource
        audioSource.volume = volume;
    }
}
