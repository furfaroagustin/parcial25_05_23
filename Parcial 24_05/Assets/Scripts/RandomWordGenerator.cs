using UnityEngine;
using UnityEngine.UI;

public class RandomWordGenerator : MonoBehaviour
{
    public string[] words; // Lista de palabras aleatorias
    public Text wordText; // Referencia al componente Text del objeto de texto en el Canvas
    public float wordCreationTime = 2f; // Tiempo entre la creaci�n de palabras (en segundos)

    private float timer = 0f; // Temporizador para rastrear el tiempo transcurrido
    private bool isGenerating = false; // Bandera para indicar si est� generando palabras

    private void Start()
    {
        StartWordGeneration(); // Iniciar la generaci�n de palabras
    }

    private void Update()
    {
        if (isGenerating)
        {
            timer += Time.deltaTime; // Incrementar el temporizador

            // Comprobar si ha pasado el tiempo de creaci�n de la siguiente palabra
            if (timer >= wordCreationTime)
            {
                GenerateRandomWord(); // Generar una nueva palabra aleatoria
                timer = 0f; // Reiniciar el temporizador
            }
        }
    }

    public void StartWordGeneration()
    {
        isGenerating = true; // Activar la generaci�n de palabras
    }

    public void StopWordGeneration()
    {
        isGenerating = false; // Detener la generaci�n de palabras
    }

    public void GenerateRandomWord()
    {
        if (words.Length > 0 && wordText != null)
        {
            int randomIndex = Random.Range(0, words.Length); // Generar un �ndice aleatorio
            string randomWord = words[randomIndex]; // Obtener la palabra aleatoria

            wordText.text = randomWord; // Establecer la palabra en el componente Text

            Debug.Log("Palabra generada: " + randomWord);
        }
        else
        {
            Debug.LogWarning("No hay palabras definidas o el componente Text no est� asignado.");
        }
    }
}

