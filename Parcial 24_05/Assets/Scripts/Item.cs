[System.Serializable]
public class Item
{
    public string name; // Nombre del elemento
    public int quantity; // Cantidad del elemento

    public Item(string name, int quantity)
    {
        this.name = name;
        this.quantity = quantity;
    }
}
