using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CuadernoScript : MonoBehaviour
{
    private List<string> notas;
    public InputField inputField;
    public GameObject panelCuaderno;
    public Text notasText;

    private string notasFilePath; // Ruta del archivo de notas

    private void Start()
    {
        panelCuaderno.SetActive(false);
        notas = new List<string>();
        notasFilePath = Application.persistentDataPath + "/notas.txt"; // Establecer la ruta del archivo de notas
        LoadNotes();
        UpdateNotesText();
    }

    public void AddNote()
    {
        string newNote = inputField.text;
        notas.Add(newNote);
        inputField.text = string.Empty;
        SaveNotes();
        UpdateNotesText();
    }

    public void ClearNotes()
    {
        notas.Clear();
        SaveNotes();
        UpdateNotesText();
    }

    private void UpdateNotesText()
    {
        string notesFormatted = "- " + string.Join("\n- ", notas);
        notasText.text = "Notas:\n" + notesFormatted;
    }

    public void AbrirCuaderno()
    {
        panelCuaderno.SetActive(true);
    }

    public void CerrarCuaderno()
    {
        panelCuaderno.SetActive(false);
    }

    private void LoadNotes()
    {
        if (File.Exists(notasFilePath))
        {
            StreamReader reader = new StreamReader(notasFilePath);
            string notesJson = reader.ReadToEnd();
            reader.Close();

            notas = JsonUtility.FromJson<List<string>>(notesJson);
        }
    }

    private void SaveNotes()
    {
        string notesJson = JsonUtility.ToJson(notas);
        StreamWriter writer = new StreamWriter(notasFilePath, false);
        writer.Write(notesJson);
        writer.Close();
    }
}