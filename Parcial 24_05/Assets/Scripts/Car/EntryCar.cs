using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryCar : MonoBehaviour
{
    public GameObject camaraVehiculo;
    public GameObject jugador;
    public bool puedoEntrar;
    public PlayerMove carMove;
    public GameObject salirVehiculo;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            entrarVehiculo();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            jugador = other.gameObject;
            puedoEntrar = true;
            if (Input.GetKeyDown(KeyCode.C))
            {
                entrarVehiculo();
            }
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            jugador = other.gameObject;
            puedoEntrar = false;
        }
    }
    public void entrarVehiculo()
    {
        if(puedoEntrar)
        {
            jugador.SetActive(false);
            camaraVehiculo.SetActive(true);
            carMove.enabled = true;

            salirVehiculo.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
