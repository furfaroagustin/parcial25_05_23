using UnityEngine;
using System.IO;
using UnityEngine.UI;
public class PersistenciaManager : MonoBehaviour
{
    public Text textComponent; // Referencia al componente Text que deseas guardar

    private string textData; // Variable de texto que se usar� para guardar en PlayerPrefs

    public void SaveTextData()
    {
        textData = textComponent.text; // Obtener el texto del componente Text
        PlayerPrefs.SetString("TextData", textData);
        PlayerPrefs.Save();
    }

    public void LoadTextData()
    {
        if (PlayerPrefs.HasKey("TextData"))
        {
            textData = PlayerPrefs.GetString("TextData");
            textComponent.text = textData; // Asignar el texto guardado al componente Text
        }
    }
}
