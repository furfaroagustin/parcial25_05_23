using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Compras : MonoBehaviour
{
    public int m_ContadorProductos;
    public Text listaText;
    public Text m_textoContador;
    public List<string> m_producto;
    public GameObject m_PanelCompras;
    private string productoSeleccionado;
    private void Start()
    {
        ActualizarLista();
        m_ContadorProductos = 0;
    }
    void Update()
    {

        m_textoContador.text = "Productos: " + m_producto.ToString();

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("SalidaSuper"))
        {
            SceneManager.LoadScene("Level");
        }
        if (collision.gameObject.CompareTag("Producto"))
        {

            if (m_producto.Count > 0)
            {
                int index = Random.Range(0, m_producto.Count);
                productoSeleccionado = m_producto[index];
                m_producto.RemoveAt(index);
                ActualizarLista();
            }
           

        }
    
    }

    public void ActualizarLista()
    {
        listaText.text = "";

        foreach (string producto in m_producto)
        {
            listaText.text += producto + "\n";
        }
    }
}