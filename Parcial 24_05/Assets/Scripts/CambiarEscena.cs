using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CambiarEscena : MonoBehaviour
{
    public string nombreEscena; // Nombre de la escena a la que se desea cambiar
    public PersistenciaManager textDataHandler; // Referencia al script que contiene la variable de texto

    void Start()
    {
        // Asegurarse de que hay un bot�n asignado en el inspector
        if (GetComponent<Button>() == null)
        {
            Debug.LogError("El script CambiarEscena necesita un componente Button adjunto al objeto.");
        }
        else
        {
            textDataHandler.LoadTextData();
            // Escuchar el evento de clic del bot�n
            GetComponent<Button>().onClick.AddListener(CambiarDeEscena);
        }
    }

    void CambiarDeEscena()
    {
        textDataHandler.SaveTextData();
        // Cambiar a la escena deseada
        SceneManager.LoadScene(nombreEscena);
    }
}

