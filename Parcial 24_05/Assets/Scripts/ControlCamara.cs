﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlCamara : MonoBehaviour
{
    public GameObject mainCamera;
    public PlayerMove P1;
    public GameObject cam;
    public GameObject panelCocina;
    public Transform m_SpawnSalidaCocina;
    private void Start()
    {
        panelCocina.SetActive(false);
        mainCamera.SetActive(false);
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cocina"))
        {
            SceneManager.LoadScene("PonerLaMesa");
           /* mainCamera.SetActive(true);
            P1.runSpeed = 0;
            cam.SetActive(false);
            panelCocina.SetActive(true);*/
           
        }
    }
    public void Cocinando()
    {
        cam.SetActive(true);
        P1.runSpeed = 7;
        mainCamera.SetActive(false);
        transform.position = m_SpawnSalidaCocina.position;
    }
}  

