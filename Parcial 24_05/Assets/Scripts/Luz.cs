using UnityEngine;
using UnityEngine.UI;

public class Luz : MonoBehaviour
{
    public Light[] lights; // Array de luces
    public Slider intensitySlider; // Slider para la intensidad
    public Dropdown colorDropdown; // Dropdown para el color

    private float maxIntensity = 10f; // Intensidad m�xima de las luces

    private Color[] colors = { Color.red, Color.green, Color.blue, Color.yellow }; // Array de colores disponibles

    private void Start()
    {
        // Asignar los controladores de eventos a los sliders y dropdown
        intensitySlider.onValueChanged.AddListener(ChangeIntensity);
        colorDropdown.onValueChanged.AddListener(ChangeColor);
    }

    private void ChangeIntensity(float value)
    {
        
            // Obtener el valor m�ximo del slider
            float sliderMaxValue = intensitySlider.maxValue;

            // Calcular la intensidad en funci�n del valor del slider y la regla de tres
            float intensity = (value / sliderMaxValue) * maxIntensity;

            // Actualizar la intensidad de cada luz en el array
            foreach (Light light in lights)
            {
                light.intensity = intensity;
            }
    }

    private void ChangeColor(int index)
    {
        // Obtener el color seleccionado del dropdown
        Color selectedColor = colors[index];

        // Actualizar el color de cada luz en el array
        foreach (Light light in lights)
        {
            light.color = selectedColor;
        }
    }
}

