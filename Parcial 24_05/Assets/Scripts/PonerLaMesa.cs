using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PonerLaMesa : MonoBehaviour
{
    private bool isDragging = false;
    private Vector3 offset;
    private Transform initialParent;
    private Vector3 initialPosition;
    private string objectName;

    public List<GameObject> objectsToInstantiate;
    public Transform instanciador;
    public Text objectNameText;

    private void Start()
    {
        // Desactiva los objetos en la lista al iniciar
        foreach (GameObject obj in objectsToInstantiate)
        {
            obj.SetActive(false);
        }

        // Instancia el primer objeto de la lista al iniciar
        InstantiateCurrentObject();

        // Muestra el nombre del objeto actual
        ShowObjectName();
    }

    private void OnMouseDown()
    {
        isDragging = true;
        offset = transform.position - GetMouseWorldPosition();
        initialParent = transform.parent;
        initialPosition = transform.position;
    }

    private void OnMouseUp()
    {
        isDragging = false;

        // Verificar si se solt� en una zona v�lida
        if (!CheckValidPosition())
        {
            ResetPosition();
        }
    }

    private void Update()
    {
        if (isDragging)
        {
            transform.position = GetMouseWorldPosition() + offset;
        }
    }

    private Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Camera.main.transform.position.z - transform.position.z;
        return Camera.main.ScreenToWorldPoint(mousePosition);
    }

    private bool CheckValidPosition()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.forward, out hit, 0.1f))
        {
            string targetZoneTag = gameObject.tag + "Zone";
            if (hit.collider.CompareTag(targetZoneTag))
            {
                // El objeto se solt� en una zona v�lida
                transform.SetParent(hit.collider.transform);
                return true;
            }
        }
        return false;
    }

    private void ResetPosition()
    {
        transform.SetParent(initialParent);
        transform.position = initialPosition;
    }

    private void InstantiateCurrentObject()
    {
        // Desactiva los objetos en la lista
        foreach (GameObject obj in objectsToInstantiate)
        {
            obj.SetActive(false);
        }

        // Instancia el objeto actual en la posici�n del instanciador y lo activa
        GameObject currentObject = objectsToInstantiate[0];
        Instantiate(currentObject, instanciador.position, Quaternion.identity);
        currentObject.SetActive(true);
        objectName = currentObject.name;
    }

    private void ShowObjectName()
    {
        // Muestra el nombre del objeto actual en el componente Text
        objectNameText.text = objectName;
    }
}
