using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daño : MonoBehaviour
{
    public Vida barraJugador;
    
    public float daño = 5.0f;
   
    public void BtSuma()
    {
        barraJugador.vidaActual += daño;

        Debug.Log("Sumo");

    }
    public void BtResta()
    {
        barraJugador.vidaActual -= daño;

        Debug.Log("Resto");

    }
    public void BtSumaM()
    {
        barraJugador.vidaActual += daño;

        Debug.Log("Sumo");

    }
    public void BtRestaM()
    {
        barraJugador.vidaActual -= daño;

        Debug.Log("Resto");

    }
    public void BtSumaS()
    {
        barraJugador.vidaActual += daño;

        Debug.Log("Sumo");

    }
    public void BtRestaS()
    {
        barraJugador.vidaActual -= daño;

        Debug.Log("Resto");

    }
}