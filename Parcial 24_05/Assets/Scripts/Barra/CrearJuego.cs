using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CrearJuego : MonoBehaviour
{
    public TextMeshProUGUI mensajeText;
    public string siguienteEscena;
    public GameObject panel;

    private void Start()
    {
        mensajeText.gameObject.SetActive(false);
    }

    public void OnCrearJuegoButton()
    {
        mensajeText.gameObject.SetActive(true);

        Invoke("CambiarEscena", 2f);
    }

    private void CambiarEscena()
    {
        panel.SetActive(false);
        SceneManager.LoadScene(siguienteEscena);
    }
}
