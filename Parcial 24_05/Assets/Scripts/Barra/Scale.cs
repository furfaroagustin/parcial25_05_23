using UnityEngine;
using UnityEngine.SceneManagement;

public class Scale: MonoBehaviour
{
    public GameObject objetoAgrandar;
    public GameObject objetoPlayer;
    public float escalaIncremento = 0.1f; // Valor de incremento de escala por segundo
    public float tiempoIncremento = 60f; // Tiempo en segundos entre cada incremento
    public string nombreEscenaDestino; // Nombre de la escena a la que quieres cambiar

    private float tiempoTranscurrido = 0f;
    private bool objetoTocado = false;

    private void Update()
    {
        tiempoTranscurrido += Time.deltaTime;

        // Verifica si ha pasado el tiempo de incremento
        if (tiempoTranscurrido >= tiempoIncremento)
        {
            // Aumenta la escala del objeto
            objetoAgrandar.transform.localScale += new Vector3(escalaIncremento, escalaIncremento, escalaIncremento);

            // Reinicia el contador de tiempo
            tiempoTranscurrido = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que colisiona es el objeto player
        if (other.gameObject == objetoPlayer)
        {
            objetoTocado = true;
            CambiarEscena();
        }
    }

    private void CambiarEscena()
    {
        // Cambia a la escena destino si el objeto ha sido tocado
        if (objetoTocado)
        {
            SceneManager.LoadScene(nombreEscenaDestino);
        }
    }
}