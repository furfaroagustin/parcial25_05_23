using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    //spawn
    //recoleccion
    // lista de compras

    public Transform m_Spawn;
    public Transform m_SpawnCarcel;
    public Transform m_SpawnSalida;
    public int m_ContadorProductos;
    public Text listaText;
    public Text m_textoContador;
    public List<string>m_producto;
    public GameObject m_PanelCompras;
    public GameObject m_PuertaDeSalida;
    public GameObject m_PanelDetencion;
    public GameObject m_PC;
    public GameObject m_Relax;
    public GameObject m_panelAviso;
    private bool isAvisoActive = false;


    private string productoSeleccionado;
    void Start()
    {
        m_PuertaDeSalida.SetActive(true);
        m_ContadorProductos = 0;
        InvokeRepeating("MostrarAviso", 5f, 30f);

        ActualizarLista();
    }

    // Update is called once per frame
    void Update()
    {
        
        m_textoContador.text = "Productos: " + m_producto.ToString();
     
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Puerta"))
        {
            SceneManager.LoadScene("Compras");
            transform.position = m_Spawn.position;
            m_PanelCompras.SetActive(true);
            m_Relax.SetActive(false);
        }
        if (other.CompareTag("SalidaSuper"))
        {
            SceneManager.LoadScene("Level");
        }
        if (other.CompareTag("Relax"))
        {
            Relax();
        }
      
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Producto"))
        {

            if (m_producto.Count > 0)
            {
                int index = Random.Range(0, m_producto.Count);
                productoSeleccionado = m_producto[index];
                m_producto.RemoveAt(index);
                ActualizarLista();
            }
            if(m_producto.Count==0)
            {
                m_PuertaDeSalida.SetActive(true);
            }
              
        }
        if(collision.gameObject.CompareTag("Salida"))
        {
            transform.position = m_SpawnSalida.position;
        }
        if (collision.gameObject.CompareTag("pc"))
        {
            PC();
        }
    }
    
    public void ActualizarLista()
    {
        listaText.text = "";

        foreach (string producto in m_producto)
        {
            listaText.text += producto + "\n";
        }
    }
    public void Carcel()
    {
        transform.position = m_SpawnCarcel.position;
        Invoke("Liberar", 40);
        m_PanelDetencion.SetActive(true);
        m_PanelCompras.SetActive(false);
    }
    void Liberar()
    {
        m_PanelDetencion.SetActive(false);
        transform.position = m_SpawnSalida.position;
    }
    public void DesactivarPC()
    {
        m_PC.SetActive(false);
    }
    void PC()
    {
        m_PC.SetActive(true);
    }
    void Relax()
    {
        m_Relax.SetActive(true);
    }
    public void DesactivarRelax()
    {
        m_Relax.SetActive(false);
    }
    void MostrarAviso()
    {
        if (!isAvisoActive)
        {
            isAvisoActive = true;
            m_panelAviso.SetActive(true);
            Invoke("OcultarAviso", 10f);
        }
    }
    void OcultarAviso()
    {
       m_panelAviso.SetActive(false);
        isAvisoActive = false;
    }
}
