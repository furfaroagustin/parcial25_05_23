using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DistanceCalculator : MonoBehaviour
{
    public Transform object1; // Transform del primer objeto
    public Transform object2; // Transform del segundo objeto
    public TextMeshProUGUI distanceText; // TextMeshProUGUI para mostrar la distancia

    void Update()
    {
        // Calcula la distancia entre los dos objetos
        float distance = Vector3.Distance(object1.position, object2.position);

        // Actualiza el texto con la distancia
        distanceText.text = "Distancia: " + distance.ToString("F2") + " metros";
    }
}
