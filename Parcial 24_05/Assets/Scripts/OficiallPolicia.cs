using UnityEngine;

public class OficiallPolicia : MonoBehaviour
{
    public float distanciaDeteccion = 5f;
    public float velocidadGiro = 5f;
    public Transform jugador;
    public GameManager gameManager;

    private bool jugadorDetectado = false;

    void Update()
    {
        // Calcula la distancia entre el oficial de polic�a y el jugador
        float distancia = Vector3.Distance(transform.position, jugador.position);

        // Si el jugador est� dentro del rango de detecci�n
        if (distancia <= distanciaDeteccion)
        {
            jugadorDetectado = true;
            DetenerJugador();
        }
        else
        {
            jugadorDetectado = false;
        }

        // Si el jugador ha sido detectado, gira hacia �l
        if (jugadorDetectado)
        {
            Vector3 direccionJugador = jugador.position - transform.position;
            Quaternion rotacionDeseada = Quaternion.LookRotation(direccionJugador);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotacionDeseada, velocidadGiro * Time.deltaTime);
        }
    }

    void DetenerJugador()
    {
        
        // Aqu� puedes implementar la l�gica para "detener" al jugador
        // Puedes desactivar su controlador de movimiento, mostrar un mensaje, etc.
        gameManager.Carcel();
        Debug.Log("�Te he detenido!");
    }
   
}
