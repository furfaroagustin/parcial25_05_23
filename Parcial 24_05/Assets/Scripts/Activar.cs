using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activar : MonoBehaviour
{
    public GameObject activar;
    public GameObject dactivar;
    public AudioSource audioSource;
    private void Start()
    {
       
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Fin"))
        {  
            activar.SetActive(true);
            dactivar.SetActive(false);
            audioSource.Play();
        }
 
    }
}
