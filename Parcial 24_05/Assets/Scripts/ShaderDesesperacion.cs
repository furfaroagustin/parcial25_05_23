using UnityEngine;

public class ShaderDeseperacion : MonoBehaviour
{
    public Material desperationMaterial;

    private void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material = desperationMaterial;
    }
}