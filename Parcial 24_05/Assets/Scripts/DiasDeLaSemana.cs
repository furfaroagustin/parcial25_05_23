using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class DiasDeLaSemana : MonoBehaviour
{
    public List<string> diasSemana;
    private int currentIndex = 0;
    public Text textoDia;
    private void Awake()
    {
        // Marcar el objeto actual como persistente entre las escenas
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        diasSemana = new List<string>()
        {
            "Lunes",
            "Martes",
            "Mi�rcoles",
            "Jueves",
            "Viernes",
            "S�bado",
            "Domingo"
        };

        CambiarComponenteLista();
  
    }
    private void Update()
    {
       // Invoke("CambiarComponenteLista", 10f);
    }

    private void CambiarComponenteLista()
    {
        // Incrementar el �ndice para pasar al siguiente componente
        currentIndex++;

        // Verificar si hemos alcanzado el final de la lista
        if (currentIndex >= diasSemana.Count)
        {
            currentIndex = 0; // Regresar al principio de la lista
        }

        // Acceder al componente actual y hacer algo con �l
        string componenteActual = diasSemana[currentIndex];
        Debug.Log("Componente actual: " + componenteActual);
        textoDia.text = componenteActual;
        // Reiniciar el proceso despu�s de 2 minutos
        Invoke("CambiarComponenteLista", 60f);
    }
}
